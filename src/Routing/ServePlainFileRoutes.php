<?php

namespace Drupal\serve_plain_file\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Dynamically generate routes for all configured served files.
 */
class ServePlainFileRoutes {

  /**
   * Provides dynamic routes for each content entity.
   */
  public function routes() {
    $collection = new RouteCollection();
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('served_file');
    /** @var \Drupal\serve_plain_file\Entity\ServedFileInterface[] $served_files */
    $served_files = $storage->loadMultiple();

    // We group the paths by route so we can pass multiple ID's that share the
    // same path.
    $routes = [];
    foreach ($served_files as $served_file) {
      $routes[$served_file->getPath()]['name'] = 'serve_plain_file.served_file.' . substr(sha1($served_file->getPath()), 0, 16) . '.route';
      $routes[$served_file->getPath()]['defaults'] = [
        '_controller' => '\Drupal\serve_plain_file\Controller\ServePlainFile::content',
        // Prevent redirects in multilingual sites to language path prefix.
        '_disable_route_normalizer' => TRUE,
      ];
      $routes[$served_file->getPath()]['requirements'] = ['_access' => 'TRUE'];
      $routes[$served_file->getPath()]['options'] = [];
    }

    foreach ($routes as $path => $item) {
      $route = new Route('/' . $path,
        $item['defaults'],
        $item['requirements'],
        $item['options']
      );
      $collection->add($item['name'], $route);
    }
    return $collection;
  }

}
