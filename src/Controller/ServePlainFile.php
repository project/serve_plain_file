<?php

namespace Drupal\serve_plain_file\Controller;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\serve_plain_file\Entity\ServedFile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Controller to serve file content.
 */
class ServePlainFile extends ControllerBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * ServePlainFile constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   LanguageManager service.
   * @param \Drupal\Component\Datetime\Time $time
   *   The Time.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match.
   */
  public function __construct(LanguageManagerInterface $language_manager, Time $time, ModuleHandlerInterface $moduleHandler, CurrentRouteMatch $currentRouteMatch) {
    $this->languageManager = $language_manager;
    $this->time = $time;
    $this->moduleHandler = $moduleHandler;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('datetime.time'),
      $container->get('module_handler'),
      $container->get('current_route_match')
    );
  }

  /**
   * Serves the content as text plain.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function content(Request $request) {
    $request_format = $request->getPreferredFormat();
    // When lupus_ce_renderer and custom_elements format,
    // redirect to the backend url path without the api prefix.
    if ($this->moduleHandler->moduleExists('lupus_ce_renderer') && $request_format == 'custom_elements') {
      // Get the path without the backend prefix.
      $path = $this->currentRouteMatch->getRouteObject()->getPath();
      $redirect_url = $request->getSchemeAndHttpHost() . $path;
      return new RedirectResponse($redirect_url);
    }
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $storage */
    $storage = $this->entityTypeManager()->getStorage('served_file');
    $langcode = $this->languageManager->getDefaultLanguage()->getId();

    $languageConfig = $this->config('language.negotiation')->get('url');
    if (
      $languageConfig !== NULL
      && isset($languageConfig['source'])
      && $languageConfig['source'] === 'domain'
    ) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
    }

    /** @var \Drupal\serve_plain_file\Entity\ServedFileInterface $served_files */
    $served_files = $storage->loadByProperties([
      'path' => ltrim($request->getPathInfo(), '/'),
      'langcode' => $langcode,
    ]);

    $served_file = NULL;
    foreach ($served_files as $item) {
      if ($item->language()->getId() === $langcode) {
        $served_file = $item;
        break;
      }
    }

    if ($served_file && $request->getPathInfo() == "/" . $served_file->getPath()) {
      $response = new Response();
      $response->setContent($served_file->getContent());

      $allowed_mime_types = (array) $this->config('serve_plain_file.settings')->get('allowed_mime_types');
      $mime_type = $served_file->getMimeType();
      $mime_type = in_array($mime_type, $allowed_mime_types) ? $mime_type : ServedFile::DEFAULT_MIME_TYPE;
      $response->headers->set('Content-Type', $mime_type);

      $max_age = $served_file->getFileMaxAge();
      $response->setPublic();
      $response->setMaxAge($max_age);

      $expires = new \DateTime();
      $expires->setTimestamp($this->time->getRequestTime() + $max_age);
      $response->setExpires($expires);

      return $response;
    }

    throw new NotFoundHttpException();
  }

}
